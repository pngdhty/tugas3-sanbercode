<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*

MAHASISWA REGISTRASI KEMUDIAN MENGISI DATA DIRI DI TABLE MAHASISWA
LAMAN REGISTRASI HANYA BISA DIAKSES OLEH ADMIN

MAHASISWA CREATE/STORE HANYA BISA DIAKSES OLEH ROLE MAHASISWA YANG BELUM MENGISI DATA DIRI (SEKALI)

MAHASISWA BISA MENGAKSES LAMAN SHOW PINJAM, TAPI HANYA MENAMPILKAN YANG DIA PINJAM SAJA
ADMIN BISA MELIHAT SELURUH MAHASISWA YANG MEMINJAM
UPDATE TANGGAL PENGEMBALIAN, STATUS ONTIME DAN MENGHAPUS PINJAMAN HANYA BISA DIAKSES OLEH ADMIN

SHOW ALL BUKU BISA DIAKSES MAHASISWA
SHOW BUKU PER ID BISA DIAKSES MAHASISWA
STORE/CREATE & HAPUS BUKU HANYA DILAKUKAN OLEH ADMIN


*/



Route::namespace('Auth')->group(function () {
    Route::post('register', 'RegisterController')->middleware('role:ADMIN');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController');
});

Route::namespace('Buku')->group(function () {
    Route::post('buku/store', 'BukuController@store'); // ADMIN MENAMBAH BUKU
    Route::delete('buku/{id}', 'BukuController@destroy'); // ADMIN MENAMBAH BUKU
    Route::patch('buku/update/{id}', 'BukuController@update'); // ADMIN MENGHAPUS BUKU
    Route::get('buku', 'BukuController@index'); // MELIHAT SEMUA BUKU
    Route::get('buku/{buku}', 'BukuController@show'); // MELIHAT BUKU BERDASARKAN KODE BUKU
});

// UNTUK CORS HANYA TERSEDIA UNTUK BUKU SHOW ALL DAN BUKU PER KODE BUKU
// 2 ROUTE TERBAWAH DARI BUKU


// Pinjam
Route::get('pinjam', 'PinjamController@index'); // ADMIN MELIHAT SEMUA LIST PINJAM
Route::post('pinjam/store', 'PinjamController@store'); // ADMIN MENAMBAH LIST PINJAM
Route::get('pinjam/{id}', 'PinjamController@show'); // MAHASISWA MELIHAT BUKU YANG DIA PINJAM BERDSARKAN ID
Route::get('pinjam/all/mahasiswa', 'PinjamController@semuaKhusus'); // MAHASISWA MELIHAT SEMUA BUKU YANG DIA PINJAM
Route::patch('pinjam/update/{id}', 'PinjamController@update'); // ADMIN MENGUPDATE PINJAM - TANGGAL PENGEMBALIAN
Route::delete('pinjam/delete/{id}', 'PinjamController@destroy'); // ADMIN MENGHAPUS PINJAM

// Mahasiswa ============  MAHASISWA CONTROLLER HANYA BISA DIAKSES MAHASISWA KARENA UNTUK MENAMBAH DATA DIRI KE TABEL MAHASISWA
Route::get('user', 'MahasiswaController@index'); // DATA DIRI MAHASISWA YANG LOGIN
Route::post('user/store', 'MahasiswaController@store'); // MAHASISWA MENAMBAH DATA DIRI KE TABLE MAHASISWA