<?php

namespace App\Http\Controllers;

use App\Http\Requests\MahasiswaRequest;
use App\Models\Mahasiswa;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class MahasiswaController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:api', 'role:MAHASISWA']);
    }

    public function mahasiswaStore()
    {
        return [
            'nim' => request('nim'),
            'fakultas' => request('fakultas'),
            'jurusan' => request('jurusan'),
            'no_hp' => request('no_WA'),
            'no_WA' => request('no_WA')
        ];
    }

    public function index()
    {
        return User::with('mahasiswa')->where('id', Auth::user()->id)->get();
    }

    public function store(MahasiswaRequest $request)
    {
        if (Gate::denies('create-mahasiswa')) {

            $mahasiswa = auth()->user()->mahasiswa()->create($this->mahasiswaStore());

            return $mahasiswa;
        }

        abort(403);
    }
}
