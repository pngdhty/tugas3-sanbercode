<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Buku extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
        // return [
        //     'judul' => $this->judul,
        //     'pengaran' => $this->pengarang,
        //     'tahun_terbit' => $this->tahun_terbit,
        // ];
    }

    public function with($request)
    {
        return ['status' => 'success'];
    }
}
