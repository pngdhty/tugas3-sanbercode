<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MahasiswaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nim' => ['required', 'integer', 'digits:6', 'unique:mahasiswas,nim'],
            'fakultas' => ['required', 'string'],
            'jurusan' => ['required', 'string'],
            'no_WA' => ['required']
        ];
    }
}
