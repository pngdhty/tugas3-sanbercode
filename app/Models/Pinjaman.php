<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Buku;

class Pinjaman extends Model
{
    protected $guarded = [];

    public function mahasiswa()
    {
        return $this->belongsTo(User::class);
    }

    public function buku()
    {
        return $this->belongsTo(Buku::class, 'kode_buku', 'kode_buku');
    }

    public function scopeGetPinjamBerlangsung($query)
    {
        return $query->whereNull('status')->whereNull('tanggal_pengembalian');
    }

    public function scopeGetPinjamSelesai($query)
    {
        return $query->whereNotNull('status')->whereNotNull('tanggal_pengembalian');
    }
}
